import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

import Index from './src/index';

export default class App extends Component {
    render() {
        return (
            <Index/>
        );
    }
}
const styles = StyleSheet.create({
    container : {
        backgroundColor:'#4EAE33',
        justifyContent:'center'
    }
});
