
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar
} from 'react-native';

export default class Sidebar extends Component {
    render() {
       return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#4EAE33"
                    barStyle="light-content"
                />
              <Text>Blank</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems:'center',
        justifyContent:'center'       
    }
});
