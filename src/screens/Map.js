import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import MapView, { PROVIDER_GOOGLE, ProviderPropType, Marker, AnimatedRegion } from 'react-native-maps';

export default class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            markers: [{ coordinates: { latitude: 3.148561, longitude: 101.652778 } }]
        }
    }

    render() {
        return (
            
                
                <View style={{ flex: 1 }}>
                    <MapView
                        provider={PROVIDER_GOOGLE}
                        style={styles.map}
                        initialRegion={{
                            latitude: 3.148561,
                            longitude: 101.652778,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                    >{this.state.markers.map(marker => (
                        <MapView.Marker
                            coordinate={marker.coordinates}
                        />
                    ))}
                    </MapView>
                </View>
         
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    }
});

const drawerStyles = {
    drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3 },
    main: { paddingLeft: 3 },
}
