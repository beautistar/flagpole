import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import { Icon } from 'native-base'
import Map from './screens/Map';
import Blank from './screens/Blank';
import SideBar from '../components/SideBar';


export const DrawerStack = DrawerNavigator({
Map: { screen:Map },
Blank: { screen: Blank }
},
{
 contentComponent: props => <SideBar {...props} />
}
)
const DrawerNavigation = StackNavigator({
 DrawerStack: { screen: DrawerStack }
}, {
 headerMode: 'float',
 navigationOptions: ({navigation}) => ({
   headerStyle: {backgroundColor: '#4EAE33'  },
   headerLeft: <TouchableOpacity style={{marginRight:35,marginLeft:20,width:'60%',padding:10}} onPress={() =>

     {if (navigation.state.index === 0) {
         navigation.navigate('DrawerOpen');
       } else {
         navigation.navigate('DrawerClose');
       }
     }
   }>
       <Icon name='ios-menu' style={{fontSize:25,color:'black'}} />
   </TouchableOpacity>
 })
})

export const PrimaryNav = StackNavigator({
 drawerStack: { screen: DrawerNavigation },
}, {
 headerMode: 'none',
 initialRouteName: 'drawerStack'
})

export default class Index extends Component {
 render() {
   return (
     <DrawerStack/>,<PrimaryNav/>
   );
 }
}