
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from 'react-native';

export default class Sidebar extends Component {
    render() {
       
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#4EAE33"
                    barStyle="light-content"
                />
                <View style={styles.btnview}>
                    <ScrollView>
                        <View>
                            <TouchableOpacity style={styles.btn} onPress={()=>{this.props.navigation.navigate('Map')}}>
                                <Text style={styles.file}>Map</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.btn} onPress={()=>{this.props.navigation.navigate('Blank')}}>
                                <Text style={styles.file}>Blank</Text>
                            </TouchableOpacity>
                        </View>
                      
                    </ScrollView>
                </View>
               

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
       
    },  
    btn:{
        borderColor:'#B2B2B2',
        padding:20,
        borderColor:'#B2B2B2',
        borderWidth: 1,
        flexDirection:'row',
        alignItems:'center'
    },
    btnview:{
        flex:0.8,
        backgroundColor: 'white',
    },
    file:{
        padding:10,
        color:'#8A8A8A',
        fontSize:15,
        fontFamily:'Avenir-Book'
    }
});
